# Nuit de l'info 2022 Projet Decathlon


## Infos

Equipe : MASTER 4.0 IA SANS IA

LIEN DU PROJET HEBERGE : http://nuit-info-decathlon.thfr.fr/

Gitlab : https://gitlab.com/thomas.fritsch2000/nuitinfo2022decathlon

## Fonctionnalités

 - Upload de fichier image / par URL
 - Recherche de sports avec l'IA mise a disposition
 - Recherche de produits en lien avec l'image
 - Information pour dire si il s'agit d'un sport aux JO 2024

## Demarrer le projet

Le projet ce lance sur docker pour plus de flexibilité.

Il y a un Dockerfile par projet (Back et Front). Le docker compose permet de tout lancer en une fois

Sur la racine :

docker-compose up -d


Sinon pour lancer chaque image indépendament :

./Front:

sudo docker build --no-cache -t nuit-info/decathlon_front .

sudo docker run -it -d -p 4031:80 --rm --name nuit-info-22-decathlon-front nuit-info/decathlon_front

./Back:

sudo docker build --no-cache -t nuit-info/decathlon_back .

sudo docker run -it -d -p 4030:3000 --rm --name nuit-info-22-decathlon-back nuit-info/decathlon_back
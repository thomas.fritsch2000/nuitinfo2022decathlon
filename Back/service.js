const axios = require('axios')
const FormData = require('form-data');
const fs = require('fs')
const path = require('path');

// Authenticate
var data_params = {
    grant_type: 'client_credentials'
}
const authenticate = async () => {
    return get_access_token();
}

module.exports.authenticate = authenticate;

async function get_access_token(){
    const res = await axios.post('https://idpdecathlon.oxylane.com/as/token.oauth2', data_params, {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Basic QzBlZjlkNjAxZjkwMWZmMDdkNWUzYzg3YjVkMmM1YmIyOWMzMzk1MGU6U2lhaGZnRkE0NDFSMzliaEJkSEFJZUpOV3Y4MFNBMVpia21pRXV6ZkYya2RIb1ZHYnRuZWR0Qkl3NU9yaTVySQ=='
        }
    });
    return res.data.access_token
}



const predict_sport = async (url, isfile) => {

    var token = await get_access_token()

    var file_param = null;


        if(isfile){
                var filepath = '';

                const directoryPath = path.join('./files');
                fs.readdir(directoryPath, function (err, files) {
                    //handling error
                    if (err) {
                        return console.log('Unable to scan directory: ' + err);
                    } 
                    //listing all files using forEach
                    filepath = files[0];
                    console.log(filepath);
                });      
                await timeout(500) 
                file_param = new FormData();

                //fs.renameSync('./files/' + filepath , './files/' + filepath + ".jpg")
        
                file_param.append('file', fs.createReadStream('./files/' + filepath), 'img.jpg'); 
        
        }else{
            file_param = {
                file: url
            }
        }
        console.log(file_param);

        // Predict sports
        await timeout(500);
        const res = await axios.post('https://api.decathlon.net/sport_vision_api/v1/sportclassifier/predict/?language=fr', file_param, {
            headers: {
                'Content-Type': 'multipart/form-data',
                'X-API-KEY' : 'b161265e-774e-4c16-ae29-024078274571',
                'Authorization': 'Bearer ' + token,
                'Accept-Encoding': 'deflate',
            }
        });
    console.log(res.data.data);

    var sports = res.data.data.sport.filter(f => f.probability > 0.1).map(e => e.name);

    //TODO: sports olympiques

    var sport = sports[0];


    if(isfile){
        var filepath = '';

        const directoryPath = path.join('./files');
        fs.readdir(directoryPath, function (err, files) {
            //handling error
            if (err) {
                return console.log('Unable to scan directory: ' + err);
            } 
            //listing all files using forEach
            filepath = files[0];
            console.log(filepath);
        });      
        await timeout(500) 
        file_param = new FormData();

        //fs.renameSync('./files/' + filepath , './files/' + filepath + ".jpg")

        file_param.append('file', fs.createReadStream('./files/' + filepath), 'img.jpg'); 

}else{
    file_param = {
        file: url
    }
}

    // Predict product
    const resP = await axios.post('https://api.decathlon.net/sport_vision_api/v1/productretrieval/predict/?language=fr', file_param, {
        headers: {
            'Content-Type': 'multipart/form-data',
            'X-API-KEY' : 'b161265e-774e-4c16-ae29-024078274571',
            'Authorization': 'Bearer ' + token,
            'Accept-Encoding': 'deflate',
        }
    });

    var produits = resP.data.data;

    fs.readdir('./files', (err, files) => {
        if (err) throw err;
      
        for (const file of files) {
          fs.unlink(path.join('./files', file), (err) => {
            if (err) throw err;
          });
        }
      });
      
    return {
        sport: sport, 
        produits_conseilles : produits
    }
}

module.exports.predict_sport = predict_sport;


function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
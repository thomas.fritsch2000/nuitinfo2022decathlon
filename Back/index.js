const express = require("express");
const bodyParser = require('body-parser');
const cors = require('cors');
const service = require('./service')
const formidable = require('formidable');

var app = express();

app.use(bodyParser.json());

app.use(cors())

const multer  = require('multer');
const upload = multer({ dest: './files' });

app.get("/", async (req, res, next) => {
    res.status(200).json({
        predict: "ALIVE"
    });
});


app.post("/predict_sport", upload.single('file'), async (req, res, next) => {
    console.log(JSON.stringify(req.body));
    res.status(200).json({
        predict: await service.predict_sport(req.body.url, true)
    });
});

app.post("/predict_sport_url", async (req, res, next) => {
    res.status(200).json({
        predict: await service.predict_sport(req.body.url, false)
    });
});

app.get("/access_token", async (req, res, next) => {
    res.status(200).json({
        token: await service.authenticate()
    });
});


app.listen(3000, () => {
 console.log("Server running on port 3000");
});
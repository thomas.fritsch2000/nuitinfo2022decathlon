import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

import axios from 'axios';

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {

  // API url
  serverUrl = environment.serverUrl;

  constructor(private http:HttpClient) { }

  async predict_sport(url: any) {
    //"https://contents.mediadecathlon.com/s858272/k$bb8bf7a4133b618a7887dfd4b1d1cf6e/dbi_bade5ce8+895c+4794+835a+8619561c071f.jpg"

    var file_param = {
      url: url
    }

    const res = await this.http.post(this.serverUrl + '/predict_sport_url', file_param, {} ).toPromise();
    console.log(res);
    return res
  }

  // Returns an observable
  upload(file: File):Observable<any> {

    // Create form data
    const formData = new FormData();

    // Store form name as "file" with file data
    formData.append("file", file, file.name);

    // Make http post request over api
    // with formData as req
    const res = this.http.post(this.serverUrl + '/predict_sport', formData, {} );
    console.log(res);
    return res
  }
}

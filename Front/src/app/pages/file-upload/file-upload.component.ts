import { Component, OnInit } from '@angular/core';
import { FileUploadService } from 'src/app/services/file-upload.service';
import {SportsList} from "../../model/sportslist";

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent implements OnInit {

    // Variable to store shortLink from api response
    shortLink: string = "";
    loading: boolean = false; // Flag variable
    file?: File; // Variable to store file
    url: string = ""
    public sport: any = "";
    public produitsConseilles: any[] = []
    public sportIsJO: string = "Ce sport fait parti des sports Olympiques";
    public sportIsNotJO: string = "Ce sport ne fait pas parti des sports Olympiques";
    public displaySportJO: string = this.sportIsNotJO;
    public pathStickman: string = "";
    public preview: any;
    public loadData: boolean = false;

    // Inject service
    constructor(private fileUploadService: FileUploadService) { }

    ngOnInit(): void {
    }

    setUrl(event :any){
      this.url = event.target.value;
    }

    // On file Select
    onChange(event:any) {
      this.file = event.target.files[0];
      this.sport = "";
      this.url = "";
      this.produitsConseilles = [];
    }

    // OnClick of button Upload
    async onUpload() {
      this.sport = "";
      this.url = "";
      this.produitsConseilles = [];
      this.loadData = true;
        this.loading = !this.loading;
        if (this.file){
           this.fileUploadService.upload(this.file).subscribe(
             (event: any) => {
                 if (typeof (event) === 'object') {
                    // Short link via api response
                   this.shortLink = event.link;
                   this.loading = false; // Flag variable
                   this.sport = event.predict.sport;
                   this.produitsConseilles = event.predict.produits_conseilles;
                   this.loadData = false;
                   if (SportsList.includes(this.sport)){
                      this.displaySportJO = this.sportIsJO;
                   }
                 }
             }
         );
      }
      else{
          var res:any = await this.fileUploadService.predict_sport(this.url)
          this.sport = res.predict.sport;
          this.produitsConseilles = res.predict.produits_conseilles;
          this.loadData = false;
          if (SportsList.includes(this.sport)){
            this.displaySportJO = this.sportIsJO;
          }
        }
    }

  goToLink(url: string){
    window.open(url, "_blank");
  }

  displayStickman(sport: string){
    switch(sport) {
      case "tir à l'arc": {
        return "../../../assets/images/Arc.png"
        break;
      }
      case "escrime": {
        return "../../../assets/images/Escrime.png"
        break;
      }
      case "football": {
        return "../../../assets/images/foot.png"
        break;
      }
      case "golf": {
        return "../../../assets/images/Golf.png"
        break;
      }
      case "lutte":
      case "judi":
      case "boxe":
      case "taekwondo":
      case "arts_martiaux":
      case "karate": {
        return "../../../assets/images/Karate.png"
        break;
      }
      case "aquathlon":
      case "aquagym":
      case "plongeon":
      case "natation": {
        return "../../../assets/images/Natation.png"
        break;
      }
      case "cyclisme bmx freestyle":
      case "cyclisme bmx racing":
      case "cyclisme sur piste":
      case "vélo hybride":
      case "vélo tout terrain": {
        return "../../../assets/images/Vtt.png"
        break;
      }
      case "tennis":
      case "badminton": {
        return "../../../assets/images/badminton.png"
        break;
      }
      case "skateboard": {
        return "../../../assets/images/Skate.png"
        break;
      }

      default: {
        return null;
        break;
      }
    }
  }
}
